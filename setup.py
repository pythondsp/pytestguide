from distutils.core import setup
from Cython.Build import cythonize

setup(
  name = 'digcom',
  ext_modules = cythonize(["math/*.pyx"], compiler_directives={
      'embedsignature': True,
      'boundscheck' : False,
      'wraparound' : False}),
)
