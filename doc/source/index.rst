.. PyTest Guide documentation master file, created by
   sphinx-quickstart on Wed Feb  1 17:20:52 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyTest Guide!
========================

Contents:

.. toctree::
   :maxdepth: 2
   :numbered:

   pytestGuide/index
   pytestDoc/index



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

